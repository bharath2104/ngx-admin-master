import {TestBed, async} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';

import {CommonModule} from '@angular/common';
import {FormInputsComponent} from './form-inputs.component';
import {ThemeModule} from '../../../@theme/theme.module';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {FormsRoutingModule} from '../forms-routing.module';

describe('FormComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FormInputsComponent],
      imports: [RouterTestingModule, CommonModule],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  it('should create the form component', async(() => {
    const fixture = TestBed.createComponent(FormInputsComponent);
    const form = fixture.debugElement.componentInstance;
    expect(form).toBeTruthy();
  }));

  /* it(`should have as title 'app'`, async(() => {
     const fixture = TestBed.createComponent(AppComponent);
     const app = fixture.debugElement.componentInstance;
     expect(app.title).toEqual('app');
   }));

   it('should render title in a h1 tag', async(() => {
     const fixture = TestBed.createComponent(AppComponent);
     fixture.detectChanges();
     const compiled = fixture.debugElement.nativeElement;
     expect(compiled.querySelector('h1').textContent).toContain('Welcome to app!');
   }));*/
});
